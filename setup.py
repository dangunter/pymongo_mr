"""
Distutils file for pymongo_mr
"""
try:
    from setuptools import setup
except:
    from distutils.core import setup
from glob import glob
import os
import sys

VERSION = '0.1'

# Main function
# -------------

setup(name = "pymongo_mr",
      version=VERSION,
      packages = ["pymongo_mr"],
      ext_modules = [],
      package_data = {},
      scripts = glob('bin/*'),
      install_requires=["pymongo >= 1.9" ],
      # metadata for upload to PyPI
      author = "Dan Gunter",
      author_email = "dkgunter@lbl.gov",
      maintainer = "Dan Gunter",
      maintainer_email = "dkgunter@lbl.gov",
      description = "Python MongoDB MapReduce",
      long_description = "Python MongoDB MapReduce",
      license = "LGPL",
      keywords = "MongoDB MapReduce",
      url = "",
      classifiers = [
        "Intended Audience :: Science/Research",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Topic :: Database",
        ],
      )
