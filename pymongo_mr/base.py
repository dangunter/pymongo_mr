# -*- coding: iso-8859-15 -*-
"""
Interface definition and default implementations
of functions for the Python MapReduce framework.

Author: Dan Gunter
Created: September 2011
"""
__rcsid__ = '$Id$'

__TEST = False

def group(coll, mapper=None, fields=[ ], extractor=None, reducer=None,
          filter={}, progress_bar=None):
    """Build aggregated records in memory.

    Args:
      coll - Input collection
      mapper - Mapper function, takes a record and returns a key
      fields - Fields to pre-extract while querying (before extractor)
      extractor - Extractor function, takes a record and returns
                  another record. If the returned record is falsy,
                  this record is skipped.
      reducer - Operates on the groups, takes a list of values
                and returns a new document.
      filter - Query filter applied to input collection
      progress_bar - Instance of ProgressBar class

    Return:
      Dictionary where key is the key returned by mapper and
      values are all extracted values for that key.
    """
    assert(mapper is not None)
    assert(reducer is not None)
    groups, n = { }, 0
    if fields:
        cursor = coll.find(filter, fields=fields)
    else:
        cursor = coll.find(filter)        
    if progress_bar:
        progress_bar.total = cursor.count()
    for record in cursor:
        n += 1
        try:
            key = mapper(record)
            if extractor:
                value = extractor(record)
                if not value:
                    continue # extractor short-circuit
            else:
                value = record
        except KeyError, err:
            g_log.warn("missing_key, key={1} _id={0}".format(
                str(record['_id']), err))
            continue
        group_record = groups.get(key, None)
        if group_record:
            group_record.append(value)
        else:
            groups[key] = [value]
        if progress_bar:
            if 0 == (n+1) % 10:
                progress_bar.update(n+1)
            if __TEST and n > 100:
                break #stop after first 100
    if progress_bar:
        progress_bar.done()
    # reduce the groups
    for k, v in groups.iteritems():
        # replace current item with reduced version
        groups[k] = reducer(v)
    return groups

def setup(**kw):
    """Called first to initialize things.
    """
    pass

def mapper(doc):
    """Return key for grouping.

    Args:
      doc - Input document dictionary
    Return:
      (str) key
    """
    return doc['_id']

"""
Fields to extract as part of the query.
"""
fields = [ '_id' ]

"""
Default filter
"""
query_filter = { }

def extractor(doc):
     """Extract/transform from input document.
     Args:
       doc - Input document dictionary
     Return:
       (dict) Output document
     """
     pass
 
extractor = None

def reducer(values):
    """
    Args:
        values - List of extracted documents for group
    Return:
        Reduced value (dictionary)
    """
    return values

